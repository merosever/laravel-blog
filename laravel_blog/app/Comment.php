<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    const IS_PUBLIC = 1;
    const IS_DRAFT = 0;

    protected $fillable = [
        'text'
    ];

    public function post() {
        return $this->hasOne(Post::class);
    }

    public function author() {
        return $this->hasOne(User::class);
    }

    public static function add($fields) {
        $comment = new static;
        $comment->fill($fields);
        $comment->save();

        return $comment;
    }

    public function edit($fields) {
        $this->fill($fields);
        $this->save();
    }

    public function remove() {
        $this->delete();
    }

    public function allow() {
        $this->status = Comment::IS_PUBLIC;
        $this->save();
    }

    public function disallow() {
        $this->status = Comment::IS_DRAFT;
    }

    public function toggleStatus($value = NULL) {
        if ($value == NULL) {
            return $this->disallow();
        }

        return $this->allow();
    }
}
